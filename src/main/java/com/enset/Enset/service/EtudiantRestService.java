/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enset.Enset.service;

import com.enset.Enset.dao.EtudiantRepository;
import com.enset.Enset.entities.Etudiant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthoritiesContainer;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jahaelle
 */
@RestController
public class EtudiantRestService {
    
    @Autowired
    private EtudiantRepository etudiantRepository; 
    
    
    @GetMapping("/test")
    public ResponseEntity<String> testUser(){
        System.out.println("-------------************ TEST *************");
        return new ResponseEntity<String>("Hello test",HttpStatus.OK);
    }
    @Secured(value={"ROLE_ADMIN","ROLE_SCOLARITE"})
    //methode qui permet d'ajouter un etudiant
    @RequestMapping (value ="/saveEtudiant", method=RequestMethod.GET)
    public Etudiant saveEtudiant(Etudiant e){
        return etudiantRepository.save(e);
         }
     @Secured(value={"ROLE_ADMIN","ROLE_SCOLARITE","ROLE_PROF","ROLE_ETUDIANT"})
   // methode qui permet de retourner une page d'etudiant
     @RequestMapping (value ="/etudiants")
    public Page<Etudiant> ListEtudiants(int page,int size){
        
        return etudiantRepository.findAll(PageRequest.of(page,size));
    }
    
    @RequestMapping(value="/getLogedUser")
    
    public Map<String,Object> getLogetUser (HttpServletRequest httpServletRequest){
        HttpSession httpSession = httpServletRequest.getSession();
        SecurityContext securityContext = 
                (SecurityContext)httpSession.getAttribute("SPRING_SECURITY_CONTEXT");
        String ursername = securityContext.getAuthentication().getName();
        
        List<String> roles = new ArrayList<>();
        for (GrantedAuthority ga: securityContext.getAuthentication().getAuthorities() ){
            roles.add(ga.getAuthority());
        }
        
        Map<String,Object> params = new HashMap<>();
        params.put("ursername", ursername);
        params.put("roles", roles);
        return params;
        
    
    }
    
}
