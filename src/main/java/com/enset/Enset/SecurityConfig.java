/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enset.Enset;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 *
 * @author jahaelle
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    @Autowired
 protected void globalConfig(AuthenticationManagerBuilder auth,DataSource dataSource) throws Exception{
     /*
     PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        authBuilder.inMemoryAuthentication().withUser("admin").password(encoder.encode("123")).roles("ADMIN","PROF");
        authBuilder.inMemoryAuthentication().withUser("prof1").password(encoder.encode("1234")).roles("PROF");
        authBuilder.inMemoryAuthentication().withUser("etd1").password(encoder.encode("123")).roles("ETUDIANT");
        authBuilder.inMemoryAuthentication().withUser("sco1").password(encoder.encode("123")).roles("SCOLARITE");
*/
     auth.jdbcAuthentication()
             .dataSource(dataSource)
             .usersByUsernameQuery("select ursername as principal, password as credential, true from users where ursername= ?")
             .authoritiesByUsernameQuery("select user_ursername as principal, roles_role as role from users_roles where user_ursername= ?")
             .rolePrefix("ROLE_");
     
                     
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                
                .csrf().disable()
                .authorizeRequests()
                    .anyRequest()
                        .authenticated()
                          .and()
                .formLogin()
                    .loginPage("/login")
                  .permitAll()
                    .defaultSuccessUrl("/index.html")
                    .failureUrl("/error.html");
        
			
    }
}
